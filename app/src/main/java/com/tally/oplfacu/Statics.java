package com.tally.oplfacu;
import java.util.ArrayList;
public class Statics {
    public static int duracionTotal=0;
    public static ArrayList<ActividadoRecurso> actividades = new ArrayList<>();
    public static  void iniciaActividadesEstaticas(){
        if(actividades.size()==0) {
            ActividadoRecurso a = new ActividadoRecurso(6, "foundation");
            actividades.add(a);
            ActividadoRecurso b = new ActividadoRecurso(4, "framing");
            actividades.add(b);
            ActividadoRecurso c = new ActividadoRecurso(5, "plumbing");
            actividades.add(c);
            ActividadoRecurso d = new ActividadoRecurso(4, "electricity");
            actividades.add(d);
            ActividadoRecurso e = new ActividadoRecurso(3, "roofing");
            actividades.add(e);
            ActividadoRecurso f = new ActividadoRecurso(3, "ceilings/walls");
            actividades.add(f);
            ActividadoRecurso g = new ActividadoRecurso(1, "fixtures");
            actividades.add(g);
            ActividadoRecurso h = new ActividadoRecurso(2, "siding");
            actividades.add(h);
            ActividadoRecurso i = new ActividadoRecurso(2, "painting");
            actividades.add(i);
            ActividadoRecurso j = new ActividadoRecurso(4, "flooring");
            actividades.add(j);
            ActividadoRecurso k = new ActividadoRecurso(1, "furniture");
            actividades.add(k);

            ActividadoRecurso ste = new ActividadoRecurso(1, "steve");
            actividades.add(ste);
            ActividadoRecurso rau = new ActividadoRecurso(1, "raul");
            actividades.add(rau);
            ActividadoRecurso bob = new ActividadoRecurso(1, "bob");
            actividades.add(bob);
            ActividadoRecurso lat = new ActividadoRecurso(1, "latisha");
            actividades.add(lat);
           //Actividad total = new Actividad(1, "Total");
            //actividades.add(total);
        }
    }
}
