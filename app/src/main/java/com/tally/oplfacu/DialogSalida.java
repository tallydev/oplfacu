package com.tally.oplfacu;


import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class DialogSalida extends DialogFragment {


    private ListView lista_acts;
    public ListViewActAdapter actAdapter;
    private ArrayList<ActividadoRecurso> actividadessss;
    private TextView tvCostos;

    class ListViewActAdapter extends ArrayAdapter<ActividadoRecurso> {

        ListViewActAdapter() {
            super(getActivity(), R.layout.salida, R.id.tv_salida012, actividadessss);
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            View item = super.getView(position, convertView, parent);
            final ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                holder.tv1 = (TextView) item.findViewById(R.id.tv_salida011);
                holder.tv2 = (TextView) item.findViewById(R.id.tv_salida012);
                holder.tv3 = (TextView) item.findViewById(R.id.tv_salida013);
                item.setTag(holder);
            } else {
                holder = (ViewHolder) item.getTag();
            }
            ActividadoRecurso actAux=actividadessss.get(position);
            // Get our View (TextView or anything) object:


            LinearLayout.LayoutParams loparams = (LinearLayout.LayoutParams) holder.tv1.getLayoutParams();
            loparams.weight = actAux.getTiempoInicio();
            holder.tv1.setLayoutParams(loparams);

            LinearLayout.LayoutParams loparams1 = (LinearLayout.LayoutParams) holder.tv2.getLayoutParams();
            loparams1.weight = actAux.getDuracion();
            holder.tv2.setLayoutParams(loparams1);
            holder.tv2.setText(actAux.getNombre());

            LinearLayout.LayoutParams loparams2 = (LinearLayout.LayoutParams) holder.tv3.getLayoutParams();
            loparams2.weight = Statics.duracionTotal-actAux.getDuracion()-actAux.getTiempoInicio();
            holder.tv3.setLayoutParams(loparams2);

            if(position<11){//es una actividad
                switch (actAux.getNombreRecurso()){
                    case "steve":
                        holder.tv2.setBackgroundColor(Color.LTGRAY);
                        break;
                    case "raul":
                        holder.tv2.setBackgroundColor(Color.RED);
                        break;
                    case "bob":
                        holder.tv2.setBackgroundColor(Color.BLUE);
                        break;
                    case "latisha":
                        holder.tv2.setBackgroundColor(Color.GREEN);
                        break;
                }
            }
            switch (position){
                case 11:
                    holder.tv2.setBackgroundColor(Color.LTGRAY);
                    break;
                case 12:
                    holder.tv2.setBackgroundColor(Color.RED);
                    break;
                case 13:
                    holder.tv2.setBackgroundColor(Color.BLUE);
                    break;
                case 14:
                    holder.tv2.setBackgroundColor(Color.GREEN);
                    break;
            }

            return (item);
        }

    }

    class ViewHolder {
        TextView tv1;
        TextView tv2;
        TextView tv3;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.dialog_actividades_salida, container);
        getDialog().setTitle("Salida");
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        int costoTotal = MainActivity.totalCost.getMin();
        /*
        Hacmeos una copia de cada arreglo de elementos, sino en cada Dialog tendriamos la referencia
        a los mismos objetos y todos los Dialog serian iguales
         */
        actividadessss=new ArrayList<>();
        for(ActividadoRecurso a : Statics.actividades){
            actividadessss.add(new ActividadoRecurso(a.getNombre(), a.getTiempoInicio(),a.getDuracion(), a.getNombreRecurso()));
        }
        lista_acts = (ListView) view.findViewById(R.id.lista_actividades);
        tvCostos= (TextView) view.findViewById(R.id.costosTotales);
        actAdapter= new ListViewActAdapter();
        lista_acts.setAdapter(actAdapter);
        tvCostos.setText("Costo Total "+costoTotal);
        return view;
    }
}