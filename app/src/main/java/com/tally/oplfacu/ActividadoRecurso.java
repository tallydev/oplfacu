package com.tally.oplfacu;


public class ActividadoRecurso {
    private int duracion;
    private int tiempoInicio;
    private int id;
    String nombre;
    String nombreRecurso;
    public static int idStatica=1000;
    boolean esRecurso;

    public ActividadoRecurso(int duracion, String nombre) {
        this.duracion = duracion;
        this.nombre = nombre;
        this.id=idStatica;
        idStatica++;
    }

    public ActividadoRecurso(String nombre, int tiempoInicio, int duracion, String nombreRecurso) {
        this.duracion = duracion;
        this.nombre = nombre;
        this.tiempoInicio=tiempoInicio;
        this.nombreRecurso=nombreRecurso;
    }

    public String getNombreRecurso() {
        return nombreRecurso;
    }

    public void setNombreRecurso(String nombreRecurso) {
        this.nombreRecurso = nombreRecurso;
    }

    public int getTiempoInicio() {
        return tiempoInicio;
    }

    public void setTiempoInicio(int tiempoInicio) {
        this.tiempoInicio = tiempoInicio;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEsRecurso() {
        return esRecurso;
    }

    public void setEsRecurso(boolean esRecurso) {
        this.esRecurso = esRecurso;
    }
}
