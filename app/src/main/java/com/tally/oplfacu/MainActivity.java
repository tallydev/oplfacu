package com.tally.oplfacu;


import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import jopt.csp.search.SearchAction;
import jopt.csp.search.SearchGoal;
import jopt.csp.variable.CspIntExpr;
import jopt.csp.variable.PropagationFailureException;
import jopt.js.JsSolver;
import jopt.js.api.search.JsSearchActions;
import jopt.js.api.variable.Activity;
import jopt.js.api.variable.JsVariableFactory;
import jopt.js.api.variable.Resource;




public class MainActivity extends AppCompatActivity {
    Button btEmpezar;
    ListView lv;
    public static CspIntExpr totalCost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inicializaVistas();
        Statics.iniciaActividadesEstaticas();

        NuevoListViewAdapter asd = new NuevoListViewAdapter(Statics.actividades);
        lv.setAdapter(asd);

        System.out.println("Termino ejecucion");
    }

    class NuevoListViewAdapter extends ArrayAdapter<ActividadoRecurso> {
        private ArrayList<ActividadoRecurso> items;
        public NuevoListViewAdapter(ArrayList<ActividadoRecurso> items) {
            super(MainActivity.this, R.layout.fila_list, R.id.tv_nombre, items);
            this.items = items;
            //tvlistas=new ArrayList<>();
        }
        public ArrayList<ActividadoRecurso> getItems() {
            return items;
        }
        public void setItems(ArrayList<ActividadoRecurso> items) {
            this.items= items;
        }
        @Override
        public View getView(int position, View convertView,
                            ViewGroup parent) {

            View item = super.getView(position, convertView, parent);

            final ViewHolderList holder;
            final ActividadoRecurso itemAux = items.get(position);
            if (convertView == null) {
                holder = new ViewHolderList();
                holder.txtVNombre=(TextView) item.findViewById(R.id.tv_nombre);
                holder.etTxDuracion=(EditText) item.findViewById(R.id.et_durac);
                holder.buttonOk=(Button) item.findViewById(R.id.boton_ok);
                item.setTag(holder);
            } else {
                holder = (ViewHolderList) item.getTag();
            }
            holder.txtVNombre.setText(itemAux.getNombre());
            holder.etTxDuracion.setText(""+itemAux.getDuracion());
            holder.buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String durString= holder.etTxDuracion.getText().toString();
                    if(durString.length()!=0){
                        int duracion=0;
                        try{
                            duracion=Integer.parseInt(durString);
                        }catch (Exception e){
                        }
                        if(duracion==0){
                            Toast.makeText(MainActivity.this,"Error con los datos",Toast.LENGTH_SHORT).show();
                        }else {
                            itemAux.setDuracion(duracion);
                            Toast.makeText(MainActivity.this,"Se cambió la duración",Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(MainActivity.this,"Faltan datos",Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return (item);
        }

    }

    static class ViewHolderList {
        TextView txtVNombre;
        EditText etTxDuracion;
        Button buttonOk;
    }

    private void inicializaVistas() {

        lv =(ListView) findViewById(R.id.lv1);
        btEmpezar=(Button) findViewById(R.id.btEmpezar);
    }

    public void empezar(View view) {
        if(Statics.actividades.size()==15) {
            Statics.duracionTotal=0;
            for (int i=0; i<11;i++){
                Statics.duracionTotal+=Statics.actividades.get(i).getDuracion();;
            }
            Toast.makeText(MainActivity.this,"La Frontera será de "+Statics.duracionTotal, Toast.LENGTH_SHORT).show();
            Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        asdrun();
                    }
                });
            t.start();
        }else {
            Toast.makeText(MainActivity.this,"Las Actividades no son 15 ", Toast.LENGTH_SHORT).show();
        }

    }


    public void asdrun() {

        //First we create an instance of the solver - used to construct and solve job scheduling problems
        JsSolver solver = JsSolver.createJsSolver();

        //Obtain a variable factory from the solver to create our variables
        JsVariableFactory varFactory = solver.getJsVarFactory();

        //Here we will set up variables for each task.
        //If the activities were all done 1 at a time, they would take no more than 35 time units to complete.
        //Since the activities will occur before 35, we will just use 0  35 as our
        //earliest possible start time and latest possible start time, respectively.
        //The ids are simply chosen to be all unique, with a = 1000, b = 1001, etc.

        int asdefg=Statics.duracionTotal;
        Activity actA = varFactory.createActivity("foundation",1000,0,asdefg,Statics.actividades.get(0).getDuracion());
        Activity actB = varFactory.createActivity("framing",1001,0,asdefg,Statics.actividades.get(1).getDuracion());
        Activity actC = varFactory.createActivity("plumbing",1002,0,asdefg,Statics.actividades.get(2).getDuracion());
        Activity actD = varFactory.createActivity("electricity",1003,0,asdefg,Statics.actividades.get(3).getDuracion());
        Activity actE = varFactory.createActivity("roofing",1004,0,asdefg,Statics.actividades.get(4).getDuracion());
        Activity actF = varFactory.createActivity("ceilings/walls",1005,0,asdefg,Statics.actividades.get(5).getDuracion());
        Activity actG = varFactory.createActivity("fixtures",1006,0,asdefg,Statics.actividades.get(6).getDuracion());
        Activity actH = varFactory.createActivity("siding",1007,0,asdefg,Statics.actividades.get(7).getDuracion());
        Activity actI = varFactory.createActivity("painting",1008,0,asdefg,Statics.actividades.get(8).getDuracion());
        Activity actJ = varFactory.createActivity("flooring",1009,0,asdefg,Statics.actividades.get(9).getDuracion());
        Activity actK = varFactory.createActivity("furniture",1010,0,asdefg,Statics.actividades.get(10).getDuracion());


        //Now, we will set up the Resources; the resources will all be available the entire time
        Resource bob = varFactory.createUnaryResource("steve",0,asdefg);
        Resource raul = varFactory.createUnaryResource("raul",0,asdefg);
        Resource steve = varFactory.createUnaryResource("bob",0,asdefg);
        Resource latisha = varFactory.createUnaryResource("latisha",0,asdefg);

        System.out.println("Resources created...");

        try {
            //We now must let the solver know of all the constraints that this problem has.
            //First we constrain the activities to start before or after other activities.
            //As you will notice there are many ways to say the same thing,
            //ie 'A' ends before 'B' starts, or 'B' starts after 'A' ends
            solver.addConstraint(actB.startsAfterEndOf(actA));

            solver.addConstraint(actC.startsAfterEndOf(actB));

            solver.addConstraint(actD.startsAfterEndOf(actB));

            solver.addConstraint(actE.startsAfterEndOf(actB));

            solver.addConstraint(actF.startsAfterEndOf(actC));
            solver.addConstraint(actF.startsAfterEndOf(actD));

            solver.addConstraint(actG.startsAfterEndOf(actF));
            solver.addConstraint(actG.startsAfterEndOf(actI));

            solver.addConstraint(actH.startsAfterEndOf(actB));

            solver.addConstraint(actI.startsAfterEndOf(actH));
            solver.addConstraint(actI.startsAfterEndOf(actF));

            solver.addConstraint(actJ.startsAfterEndOf(actI));

            solver.addConstraint(actK.startsAfterEndOf(actJ));

            System.out.println("Temporal constraints created...");

            //Also we will let each activity know which resources it can use.
            //In each case, it will only need 1 unit of capacity
            solver.addConstraint(actA.require(new Resource[]{steve},1));
            solver.addConstraint(actB.require(new Resource[]{steve,raul},1));
            solver.addConstraint(actC.require(new Resource[]{bob},1));
            solver.addConstraint(actD.require(new Resource[]{bob},1));
            solver.addConstraint(actE.require(new Resource[]{steve,raul},1));
            solver.addConstraint(actF.require(new Resource[]{raul,latisha},1));
            solver.addConstraint(actG.require(new Resource[]{bob, latisha},1));
            solver.addConstraint(actH.require(new Resource[]{raul},1));
            solver.addConstraint(actI.require(new Resource[]{latisha, raul},1));
            solver.addConstraint(actJ.require(new Resource[]{latisha},1));
            solver.addConstraint(actK.require(new Resource[]{latisha},1));

            System.out.println("FC constraints created...");

            //Now to get the first solution
            //Get the pre-defined search actions
            JsSearchActions actions = solver.getJsSearchActions();

            //We'll attempt to locate our optimal solution by assigning a resource
            //and a start time to each of the 11 activities
            Activity[] activities = new Activity[]{actA, actB, actC, actD, actE, actF, actG, actH, actI, actJ, actK};
            SearchAction action = actions.generateResourceAndStartTimes(activities);


            System.out.println("actions created...");

            //We will want to quantify the cost of this job.  For creativity's sake, let's say that every resource
            //but Bob charges per time unit of work whereas Bob charges based on the number of jobs that he works on.
            //Let us set up the following rates for labor:
            // Bob charges 120 / job
            // Raul charges 80 / time unit
            // Steve charges 30 / time unit
            // Latisha charges 95 / time unit

            //We will now set up the cost for each person ...
            //Bob is based of the number of operations that he accomplishes
            CspIntExpr costOfBob = bob.getNumOperationsExpr().multiply(120);
            //The makespan is the length of time a resource is used -
            //from the beginning of the first activity to the end of the last activity.
            //Thus, the cost of the remaining resources is the makespan * resource cost per unit of time.
            CspIntExpr costOfRaul = raul.getMakeSpanExpr().multiply(80);
            CspIntExpr costOfSteve= steve.getMakeSpanExpr().multiply(30);
            CspIntExpr costOfLatisha = latisha.getMakeSpanExpr().multiply(95);

            //The total cost, then, of building the house, is the sum of the costs of all 4 resources
            totalCost = costOfBob.add(costOfRaul).add(costOfSteve).add(costOfLatisha);

            //CspIntExpr qwe=activities[10].getEndTimeExpr();

            System.out.println("total cost created...");

            //Our intention, our goal, is to minimize the cost of building the house
            //When we "strictly" minimize, it means we will only find solutions that are better than a previous solution
            //(rather than finding solutions that are either as good or better than previous solutions).
            SearchGoal goal = solver.getJsSearchGoals().strictlyMinimize(totalCost, 1);

            System.out.println("this goal created...");

            Date then = new Date();
            //Indicate to the solver that we are done setting up the problem.
            solver.problemBuilt();
            System.out.println("About to propagate...");
            solver.propagate();
            System.out.println("About to solve...");

            //This will get the first viable solution.
            solver.solve(action, goal, true);

            Resource[] resources = new Resource[]{steve, raul, bob, latisha};
            //Here we will print the first solution that we obtain
            //Print activities
            for (int i=0; i<activities.length; i++) {
                System.out.println("OPL "+activities[i].toString());

            }
            //Print resources
            for (int i=0; i<resources.length; i++) {
                System.out.println("OPL "+resources[i].toString());
            }
            System.out.println("OPL Total cost: "+totalCost.getMin()+".."+totalCost.getMax());
            System.out.println("OPL runTime: " + ((new Date()).getTime() - then.getTime()) + "\n");



            //Now we look for better solutions.
            //We print the costs as we go, to ensure that we are actually getting better
            while (solver.nextSolution()) {
                for (int i=0; i<resources.length; i++) {
                    String aux=resources[i].getBeginTimeExpr().toString();
                    aux.replace(resources[i].getName(),"");
                    CspIntExpr asd =resources[i].getNumOperationsExpr();
                    asd.getMin();
                }

                for (int i=0; i<activities.length; i++) {
                    int asd[]= activities[i].getRequiredResources();
                    String s=resources[asd[0]-1].getName();
                    Statics.actividades.get(i).setNombreRecurso(s);
                    Statics.actividades.get(i).setTiempoInicio(activities[i].getStartTimeExpr().getMin());
                    Statics.actividades.get(i).setDuracion(activities[i].getDurationExpr().getMin());
                }
                for (int i=0; i<resources.length; i++) {
                    ActividadoRecurso recurso =Statics.actividades.get(i + 11);
                    recurso.setTiempoInicio(resources[i].getBeginTimeExpr().getMin());
                    recurso.setDuracion(resources[i].getMakeSpanExpr().getMin());
                    switch (i+11){
                        case 11:
                            recurso.setNombre("Steve "+costOfSteve.getMin());
                            break;
                        case 12:
                            recurso.setNombre("Raul "+costOfRaul.getMin());
                            break;
                        case 13:
                            recurso.setNombre("Bob "+costOfBob.getMin());
                            break;
                        case 14:
                            recurso.setNombre("Latis "+costOfLatisha.getMin());
                            break;
                    }
                }
                FragmentManager manager = getFragmentManager();
                DialogSalida dialogSalida = new DialogSalida();
                dialogSalida.show(manager, "dialogSalida");
            }
        }
        catch (PropagationFailureException pfe) {
            // The solver attempts to 'propagate' the constraints immediately;
            // that is, it attempts to make the variable values consistent with
            // those allowed by the constraints.
            // Propagation should not fail in this particular example.
            System.out.println("Constraint was impossible to satisfy");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
