package jopt.csp.search;

import jopt.csp.solution.SolverSolution;

/**
 * Contains a set of neighboring solutions that are related to an initial
 * solution.  The neighbor can define solutions to variables that should 
 * not be updated from the original solution as well as values to assign 
 * to variables not in the scope of the original solution.  Each neighbor 
 * will be applied as an alternative solution to produce a set of choices.
 * 
 * @author Nick Coleman
 * @version $Revision: 1.5 $
 */
public interface Neighborhood {
    /**
     * Sets the initial solution to which this neighborhood is related
     */
    public void setInitialSolution(SolverSolution initial);
    
	/**
     * Returns the number of potential solutions contained in the neighborhood
	 */
    public int size();
    
    /**
     * Returns the neighboring solution at index <code>i</code>
     * @param i     Index of neighbor within neighborhood
     * @return      Solution given by the neighbor or null if some pre-propagation
     *              checks show that the neighbor is invalid (and will fail constraint
     *              satisfaction).
     */
    public SolverSolution getNeighbor(int i);
    
    /**
     * Indicates that a neighbor has been selected from this neighborhood
     * and is about to be used to update the initial solution.  This is useful
     * if the neighborhood will adjust the order in which neighbors are produced.
     */
    public void neighborSelected(int i);
    
    /**
     * Returns the Neighborhood represented by the latest selected Neighborhood  
     * @return Neighborhood represented by the latest selected Neighborhood
     * 			null if no such neighbor has been selected
     */
	public Neighborhood getSelectedNeighborhood();
    
	/**
     * Returns the offset into Neighborhood represented by the latest selected Neighborhood  
     * @return the offset into Neighborhood represented by the latest selected Neighborhood
     * 			-1 if no such neighbor has been selected
     */
    public int getSelectedNeighborhoodOffset();
}
