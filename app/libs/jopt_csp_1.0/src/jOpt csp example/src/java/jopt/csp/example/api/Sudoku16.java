package jopt.csp.example.api;

import java.util.ArrayList;

import jopt.csp.CspSolver;
import jopt.csp.spi.arcalgorithm.constraint.num.global.NumAllDiffConstraint;
import jopt.csp.spi.arcalgorithm.variable.IntExpr;
import jopt.csp.variable.CspConstraint;
import jopt.csp.variable.CspIntVariable;
import jopt.csp.variable.CspVariableFactory;

/**
 * Uses the CSP solver to solve a hard-coded size "16" Sudoku puzzle
 */
public class Sudoku16 {
    public final static int A=11;
    public final static int B=12;
    public final static int C=13;
    public final static int D=14;
    public final static int E=15;
    public final static int F=16;
    public final static int O=10;
    
    public static void main(String args[]) throws Exception {
	    
        CspSolver solver = CspSolver.createSolver();
        CspVariableFactory varFactory = solver.getVarFactory();
        solver.setAutoPropagate(true);
        
        // create empty matrix
		CspIntVariable cells[][] = new CspIntVariable[16][];
        for (int i=0; i<16; i++)
            cells[i] = new CspIntVariable[16];
        
        // initialize all cells to domains 1..16
        for (int i=0; i<16; i++)
            for (int j=0; j<16; j++)
                cells[i][j] = varFactory.intVar("cell [" + i + ", " + j + "]", 1, 16);
        
        // row constraints
        for (int i=0; i<16; i++) {
            CspIntVariable[] row = cells[i];
            allDiff(solver, row);
        }
        
        // column constraints
        ArrayList columnList = new ArrayList();
        for (int j=0; j<16; j++) {
            // build list of cells in column j
        	columnList.clear();
            for (int i=0; i<16; i++)
                columnList.add(cells[i][j]);
            
            // convert list to array
            CspIntVariable[] column = (CspIntVariable[]) columnList.toArray(new CspIntVariable[columnList.size()]);
            
            // add constraint
            allDiff(solver, column);
        }
        for (int i=0; i<16; i+=4){
            for (int j=0; j<16; j+=4) {
                CspIntVariable square[] = new CspIntVariable[]{
                		cells[j+0][i+0], cells[j+0][i+1], cells[j+0][i+2],cells[j+0][i+3],
                        cells[j+1][i+0], cells[j+1][i+1], cells[j+1][i+2],cells[j+1][i+3],
                        cells[j+2][i+0], cells[j+2][i+1], cells[j+2][i+2],cells[j+2][i+3],
                        cells[j+3][i+0], cells[j+3][i+1], cells[j+3][i+2],cells[j+3][i+3],
                };
                allDiff(solver, square);        
            }
        }
        
        cells[0][0].setValue(O);
        cells[0][1].setValue(B);
        cells[0][2].setValue(D);
        cells[0][11].setValue(1);
        
        cells[1][0].setValue(5);
        cells[1][1].setValue(4);
        cells[1][6].setValue(6);
        cells[1][8].setValue(F);
        cells[1][9].setValue(8);
        cells[1][10].setValue(O);
        cells[1][13].setValue(1);
        cells[1][14].setValue(9);
        
        cells[2][1].setValue(6);
        cells[2][2].setValue(1);
        cells[2][3].setValue(F);
        cells[2][4].setValue(B);
        cells[2][6].setValue(C);
        cells[2][9].setValue(4);
        cells[2][12].setValue(2);
        cells[2][13].setValue(3);
        
        cells[3][2].setValue(3);
        cells[3][3].setValue(9);
        cells[3][10].setValue(B);
        cells[3][11].setValue(5);
        cells[3][12].setValue(A);
        cells[3][13].setValue(D);
        cells[3][15].setValue(O);
        
        cells[4][6].setValue(8);
        cells[4][7].setValue(D);
        
        cells[5][0].setValue(3);
        cells[5][5].setValue(6);
        cells[5][6].setValue(1);
        cells[5][7].setValue(9);
        cells[5][9].setValue(O);
        cells[5][10].setValue(5);
        cells[5][11].setValue(2);
        cells[5][12].setValue(4);
        cells[5][13].setValue(C);
        
        cells[6][2].setValue(C);
        cells[6][3].setValue(5);
        cells[6][4].setValue(3);
        cells[6][5].setValue(O);
        cells[6][6].setValue(E);
        cells[6][8].setValue(1);
        cells[6][10].setValue(D);
        cells[6][11].setValue(8);
        cells[6][14].setValue(B);
        
        cells[7][0].setValue(E);
        cells[7][1].setValue(9);
        cells[7][7].setValue(A);
        cells[7][10].setValue(4);
        cells[7][12].setValue(7);
        cells[7][13].setValue(O);
        cells[7][14].setValue(D);
        
        cells[8][1].setValue(2);
        cells[8][2].setValue(B);
        cells[8][3].setValue(D);
        cells[8][5].setValue(E);
        cells[8][8].setValue(A);
        cells[8][14].setValue(3);
        cells[8][15].setValue(9);
        
        cells[9][1].setValue(5);
        cells[9][4].setValue(9);
        cells[9][5].setValue(7);
        cells[9][7].setValue(4);
        cells[9][9].setValue(F);
        cells[9][10].setValue(1);
        cells[9][11].setValue(6);
        cells[9][12].setValue(E);
        cells[9][13].setValue(8);
        
        cells[10][2].setValue(F);
        cells[10][3].setValue(3);
        cells[10][4].setValue(1);
        cells[10][5].setValue(5);
        cells[10][6].setValue(D);
        cells[10][8].setValue(B);
        cells[10][9].setValue(7);
        cells[10][10].setValue(C);
        cells[10][15].setValue(6); 
        
        cells[11][8].setValue(O);
        cells[11][9].setValue(3);
        
        cells[12][0].setValue(F);
        cells[12][2].setValue(4);
        cells[12][3].setValue(C);
        cells[12][4].setValue(A);
        cells[12][5].setValue(1);
        cells[12][12].setValue(5);
        cells[12][13].setValue(2);

        cells[13][2].setValue(9);
        cells[13][3].setValue(6);
        cells[13][6].setValue(B);
        cells[13][9].setValue(1);
        cells[13][11].setValue(O);
        cells[13][12].setValue(D);
        cells[13][13].setValue(A);
        cells[13][14].setValue(C);
        
        cells[14][1].setValue(7);
        cells[14][2].setValue(8);
        cells[14][5].setValue(2);
        cells[14][6].setValue(9);
        cells[14][7].setValue(E);
        cells[14][9].setValue(C);
        cells[14][14].setValue(1);
        cells[14][15].setValue(4);

        cells[15][4].setValue(C);
        cells[15][13].setValue(7);
        cells[15][14].setValue(F);
        cells[15][15].setValue(3);
        
        ArrayList allUnbound = new ArrayList();
        for (int i=0; i<16; i++) {
            for (int j=0; j<16; j++) {
                if (!cells[i][j].isBound()) {
                    allUnbound.add(cells[i][j]);    
                }
            }
        }

        // create list of all cells
        ArrayList allList = new ArrayList();
        for (int i=0; i<16; i++)
            for (int j=0; j<16; j++)
            	allList.add(cells[i][j]);

        CspIntVariable[] allCells = (CspIntVariable[]) allUnbound.toArray(new CspIntVariable[allUnbound.size()]);
        
        if (solver.solve(allCells, false)) {
            dump(cells);
            
            while (solver.nextSolution())
                dump(cells);
            
            System.out.println("all solutions found");
        }
        else
            System.out.println("no solution found!");
   }
    
   private static void allDiff(CspSolver solver, CspIntVariable vars[]) throws Exception {
       IntExpr[] intVars = new IntExpr[vars.length];
       for (int i=0; i<vars.length; i++) {
           intVars[i] = (IntExpr)vars[i];
       }
       
       CspConstraint constraint = new NumAllDiffConstraint(intVars);
       solver.addConstraint(constraint);
   }
   
   private static void dump(CspIntVariable cells[][]) {
        for (int i=0; i<16; i++) {
            for (int j=0; j<16; j++) {
                int min = cells[i][j].getMin();
                int max = cells[i][j].getMax();
                
                System.out.print("[");
                if (min==max) {
                    switch(min){
	                    case A:
	                        System.out.print("A");
	                    	break;
	                    case(B):
	                        System.out.print("B");
	                    	break;
	                    case(C):
	                        System.out.print("C");
	                		break;
	                    case(D):
	                        System.out.print("D");
	                		break;
	                    case(E):
	                        System.out.print("E");
	                		break;
	                    case(F):
	                        System.out.print("F");
	                		break;
	                    case(O):
	                        System.out.print("O");
	                		break;
	                    default:    
	                        System.out.print(min);
                    }
                }
                else {
                    System.out.print(min);
                    System.out.print("..");
                    System.out.print(max);
                }
                System.out.print("]\t");
            }
            
            System.out.println();
        }
        
        System.out.println();
        System.out.println();
   }

}