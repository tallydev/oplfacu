package jopt.csp.example.api;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import jopt.csp.CspSolver;
import jopt.csp.solution.SolutionScope;
import jopt.csp.solution.SolverSolution;
import jopt.csp.spi.arcalgorithm.constraint.num.global.NumAllDiffConstraint;
import jopt.csp.spi.arcalgorithm.variable.IntExpr;
import jopt.csp.spi.solver.ChoicePointStack;
import jopt.csp.variable.CspConstraint;
import jopt.csp.variable.CspIntVariable;
import jopt.csp.variable.CspVariableFactory;
import jopt.csp.variable.PropagationFailureException;

/**
 * Uses the CSP solver to work with Sudoku puzzles.
 * 
 * Implementation supports a GUI-based Sudoku as well as a hard-coded example.
 */
public class Sudoku implements ActionListener {
    static CspIntVariable cells[][] = new CspIntVariable[9][];
    static JComponent combos[][] = new JComponent[9][9];
    static JFrame frame = new JFrame("Sudoku");
    static JPanel panel = new JPanel();
    static CspSolver solver;
    static ChoicePointStack stack;
    static SolutionScope scope;
    
    public Sudoku(){
        init();
    }
    
    private void init(){
        try{
            solver = CspSolver.createSolver();
            CspVariableFactory varFactory = solver.getVarFactory();
            solver.setAutoPropagate(false);
            
            // create empty matrix
            
            for (int i=0; i<9; i++)
                cells[i] = new CspIntVariable[9];
            
            // initialize all cells to domains 1..9
            for (int i=0; i<9; i++)
                for (int j=0; j<9; j++)
                    cells[i][j] = varFactory.intVar("cell [" + i + ", " + j + "]", 1, 9);
            
            // row constraints
            for (int i=0; i<9; i++) {
                CspIntVariable[] row = cells[i];
                allDiff(solver, row);
            }
            
            // column constraints
            ArrayList columnList = new ArrayList();
            for (int j=0; j<9; j++) {
                // build list of cells in column j
                columnList.clear();
                for (int i=0; i<9; i++)
                    columnList.add(cells[i][j]);
                
                // convert list to array
                CspIntVariable[] column = (CspIntVariable[]) columnList.toArray(new CspIntVariable[columnList.size()]);
                
                // add constraint
                allDiff(solver, column);
            }
            
            // square [0, 0]
            CspIntVariable square[] = new CspIntVariable[]{
                    cells[0][0], cells[0][1], cells[0][2],
                    cells[1][0], cells[1][1], cells[1][2],
                    cells[2][0], cells[2][1], cells[2][2]
            };
            allDiff(solver, square);
            
            // square [0, 1]
            square = new CspIntVariable[]{
                    cells[0][3], cells[0][4], cells[0][5],
                    cells[1][3], cells[1][4], cells[1][5],
                    cells[2][3], cells[2][4], cells[2][5]
            };
            allDiff(solver, square);
            
            // square [0, 2]
            square = new CspIntVariable[]{
                    cells[0][6], cells[0][7], cells[0][8],
                    cells[1][6], cells[1][7], cells[1][8],
                    cells[2][6], cells[2][7], cells[2][8]
            };
            allDiff(solver, square);
            
            // square [1, 0]
            square = new CspIntVariable[]{
                    cells[3][0], cells[3][1], cells[3][2],
                    cells[4][0], cells[4][1], cells[4][2],
                    cells[5][0], cells[5][1], cells[5][2]
            };
            allDiff(solver, square);
            
            // square [1, 1]
            square = new CspIntVariable[]{
                    cells[3][3], cells[3][4], cells[3][5],
                    cells[4][3], cells[4][4], cells[4][5],
                    cells[5][3], cells[5][4], cells[5][5]
            };
            allDiff(solver, square);
            
            // square [1, 2]
            square = new CspIntVariable[]{
                    cells[3][6], cells[3][7], cells[3][8],
                    cells[4][6], cells[4][7], cells[4][8],
                    cells[5][6], cells[5][7], cells[5][8]
            };
            allDiff(solver, square);
            
            // square [2, 0]
            square = new CspIntVariable[]{
                    cells[6][0], cells[6][1], cells[6][2],
                    cells[7][0], cells[7][1], cells[7][2],
                    cells[8][0], cells[8][1], cells[8][2]
            };
            allDiff(solver, square);
            
            // square [2, 1]
            square = new CspIntVariable[]{
                    cells[6][3], cells[6][4], cells[6][5],
                    cells[7][3], cells[7][4], cells[7][5],
                    cells[8][3], cells[8][4], cells[8][5]
            };
            allDiff(solver, square);
            
            // square [2, 2]
            square = new CspIntVariable[]{
                    cells[6][6], cells[6][7], cells[6][8],
                    cells[7][6], cells[7][7], cells[7][8],
                    cells[8][6], cells[8][7], cells[8][8]
            };
            allDiff(solver, square);
            
            scope = new SolutionScope();
            for (int i=0;i<cells.length;i++) {
                for (int j=0;j< cells[i].length; j++){
                    scope.add(cells[i][j]);
                }
            }
            
            // enter initial values
            cells[0][0].setValue(2);
            cells[0][2].setValue(1);
            cells[0][3].setValue(9);
            
            cells[1][0].setValue(6);
            cells[1][4].setValue(5);
            cells[1][5].setValue(8);
            cells[1][7].setValue(4);
            cells[1][8].setValue(1);
            
            cells[2][5].setValue(3);
            cells[2][7].setValue(5);
            
            cells[3][1].setValue(1);
            cells[3][7].setValue(7);
            
            cells[4][1].setValue(2);
            cells[4][2].setValue(4);
            cells[4][3].setValue(3);
            cells[4][5].setValue(5);
            cells[4][6].setValue(1);
            cells[4][7].setValue(8);
            
            cells[5][1].setValue(7);
            cells[5][7].setValue(2);
            
            cells[6][1].setValue(6);
            cells[6][3].setValue(5);
            
            cells[7][0].setValue(7);
            cells[7][1].setValue(9);
            cells[7][3].setValue(2);
            cells[7][4].setValue(4);
            cells[7][8].setValue(5);
            
            cells[8][5].setValue(7);
            cells[8][6].setValue(4);
            cells[8][8].setValue(2);
            
            solver.propagate();
            
            ArrayList unbound = new ArrayList();
            for (int i=0; i<9; i++) {
                for (int j=0; j<9; j++) {
                    if (!cells[i][j].isBound()) {
                        unbound.add(cells[i][j]);    
                    }
                }
            }
            
            CspIntVariable[] allCells = (CspIntVariable[]) unbound.toArray(new CspIntVariable[unbound.size()]);
            
            if (solver.solve(allCells, false)) {
                dumpVerbose(cells);
                while (solver.nextSolution())
                    dumpVerbose(cells);
                
                System.out.println("all solutions found");
                
            }
            else
                System.out.println("no solution found!");
        }
        catch(Exception pfe) {
            System.err.println(pfe.getStackTrace());
        }
    }
    
    public static void main(String args[]) throws Exception {
        
        Sudoku sdk = new Sudoku();
//        sdk.createAndShowGUI();
        
    }
    
    
    private static void allDiff(CspSolver solver, CspIntVariable vars[]) throws Exception {
        IntExpr[] intVars = new IntExpr[vars.length];
        for (int i=0; i<vars.length; i++) {
            intVars[i] = (IntExpr)vars[i];
        }
        CspConstraint constraint = new NumAllDiffConstraint(intVars);
        solver.addConstraint(constraint);
    }
    
    public void actionPerformed(ActionEvent e) {
        JComboBox cb = (JComboBox)e.getSource();
        String number = (String)cb.getSelectedItem();
        if (number.equals(" ")) {
            return;
        }
        for (int i=0; i<combos.length;i++) {
            for (int j=0; j<combos[i].length;j++) {
                
                if (combos[i][j]==cb) {
                    try {
                        solver.setAutoPropagate(false);
                        SolverSolution backup = solver.storeSolution(scope);
                        cells[i][j].setValue(Integer.parseInt(number));
                        if (!solver.propagate()) {
                            JOptionPane.showMessageDialog(null, "The last selection led to an invalid puzzle state", "Failure", JOptionPane.ERROR_MESSAGE);
                            solver.restoreSolution(backup,true);
                            cells[i][j].removeValue(Integer.parseInt(number));
                            if (!solver.propagate()) {
                                JOptionPane.showMessageDialog(null, "The problem has reached an unstable state, please start over.", "Failure", JOptionPane.ERROR_MESSAGE);   
                            }
                        }
                    }
                    catch(PropagationFailureException pfe) {
                        JOptionPane.showMessageDialog(null, "The last selection led to an invalid puzzle state", "PropationFailureException", JOptionPane.ERROR_MESSAGE);
                        pfe.printStackTrace();
                    }
                    
                    createAndShowGUI();
                    return;
                }
            }
        }
        
        
        
    }
    
    private  void createAndShowGUI() {
        panel.invalidate();
        
        JFrame.setDefaultLookAndFeelDecorated(true);
        panel.removeAll();
        
        //Create and set up the window    
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel emptyLabel = new JLabel("");
        emptyLabel.setPreferredSize(new Dimension(175, 100));
        frame.getContentPane().add(emptyLabel, BorderLayout.CENTER);
        
        panel.setLayout(new GridLayout(3,3));
        JPanel[][] panelGrid = new JPanel[3][3];
        for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
                panelGrid[i][j]=new JPanel();
                panelGrid[i][j].setLayout(new GridLayout(3,3));
            }
        }
        
        boolean allBound=true;
        for (int i=0; i<cells.length; i++) {
            for (int j=0; j<cells[i].length;j++){
                if (cells[i][j].isBound()) {
                    JLabel label =new JLabel(""+cells[i][j].getMin(),SwingConstants.CENTER); 
                    label.setBorder(new LineBorder(Color.GRAY));
                    combos[i][j]=label;
                    panelGrid[i/3][j/3].add(label);
                }
                else {
                    allBound=false;
                    String[] nums = new String[cells[i][j].getSize()+1];
                    nums[0]=" ";
                    int num = cells[i][j].getMin();
                    for (int k=1;k<cells[i][j].getSize()+1;k++) {
                        nums[k]=""+num;
                        num = cells[i][j].getNextHigher(num);
                    }
                    JComboBox numList = new JComboBox(nums);
                    numList.addActionListener(this);
                    combos[i][j]=numList;
                    panelGrid[i/3][j/3].add(numList);
                }
            }
        }
        if (allBound) {
            JOptionPane.showMessageDialog(null, "The last selection gives a fully solvable problem", "Problem Solved", JOptionPane.INFORMATION_MESSAGE);
        }
        
        for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
                panelGrid[i][j].setBorder(new BevelBorder(BevelBorder.RAISED));
                panel.add(panelGrid[i][j]);
            }
        }
        
        frame.getContentPane().add(panel);
        panel.repaint();
        panel.setVisible(true);
        panel.validate();
        
        //Display the window.
        frame.validate();
        
        frame.getContentPane().repaint();
        frame.setVisible(true);
        frame.setSize(500,500);
        frame.setResizable(false);
        
    }
    
    private static void dumpVerbose(CspIntVariable cells[][]) {
        for (int i=0; i<9; i++) {
            for (int j=0; j<9; j++) {
                System.out.println(cells[i][j]);
            }
        }
        
        System.out.println();
    }
}