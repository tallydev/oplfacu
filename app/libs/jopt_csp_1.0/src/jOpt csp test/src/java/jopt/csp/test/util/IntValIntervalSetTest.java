package jopt.csp.test.util;

import jopt.csp.util.IntIntervalSet;
import jopt.csp.util.IntValIntervalSet;
import junit.framework.TestCase;

/**
 * @author Chris Johnson
 */
public class IntValIntervalSetTest extends TestCase {
    
    IntValIntervalSet set;
	public IntValIntervalSetTest(String testName) {
        super(testName);
    }
    
    public void setUp() {
        set = new IntValIntervalSet();
    }
    
    public void tearDown() {
        set = null;
    }
    
    public void testValIntervalEmbeddedAdd() {
        set.add(0, 100,4);
        
        assertEquals("min is 0", 0, set.getMin());
        assertEquals("max is 100", 100, set.getMax());
        
        set.add(20,40,5);
        
        // set contains everything between 0 and 100 inclusive
        for (int i=0; i<=100; i++) {
            assertTrue("set contains "+i, set.contains(i));
        }
        
        assertEquals("size is 101", 101, set.size());
        
        int firstIdx = set.getFirstIntervalIndex();
        int lastIdx = set.getLastIntervalIndex();
        assertEquals("set contains a single interval", 2, lastIdx-firstIdx);
        assertEquals(set.getWorth(firstIdx),4);
        assertEquals(set.getWorth(firstIdx+1),9);
        assertEquals(set.getWorth(lastIdx),4);
    }
    
//    public void testValIntervalEmbeddedAddZero() {
//        set.add(0, 100,0);
//        
//        assertEquals("min is 0", 0, set.getMin());
//        assertEquals("max is 100", 100, set.getMax());
//        
//        set.add(20,40,5);
//        
//        // set contains everything between 0 and 100 inclusive
//        for (int i=0; i<=100; i++) {
//            assertTrue("set contains "+i, set.contains(i));
//        }
//        
//        assertEquals("size is 101", 101, set.size());
//        
//        int firstIdx = set.getFirstIntervalIndex();
//        int lastIdx = set.getLastIntervalIndex();
//        int counter=1;
//        int curIdx=firstIdx;
//        while (curIdx!=lastIdx){
//        	curIdx = set.getNextIntervalIndex(curIdx);
//        	counter++;
//        }
//        assertEquals(counter, 3);
//        assertEquals(set.getWorth(firstIdx),0);
//        assertEquals(set.getWorth(firstIdx+1),5);
//        assertEquals(set.getWorth(lastIdx),0);
//        
//        assertEquals(set.getIntervalStart(firstIdx),0);
//        assertEquals(set.getIntervalStart(firstIdx+1),20);
//        assertEquals(set.getIntervalStart(lastIdx),41);
//        
//        assertEquals(set.getIntervalEnd(firstIdx),19);
//        assertEquals(set.getIntervalEnd(firstIdx+1),40);
//        assertEquals(set.getIntervalEnd(lastIdx),100);
//    }
    
    public void testValIntervalZeroEmbeddedAdd() {
        set.add(0, 100,4);
        
        assertEquals("min is 0", 0, set.getMin());
        assertEquals("max is 100", 100, set.getMax());
        
        set.add(20,40,0);
        
        // set contains everything between 0 and 100 inclusive
        for (int i=0; i<=100; i++) {
            assertTrue("set contains "+i, set.contains(i));
        }
        
        assertEquals("size is 101", 101, set.size());
        
        int firstIdx = set.getFirstIntervalIndex();
        int lastIdx = set.getLastIntervalIndex();
        assertEquals("set contains a single interval",lastIdx,firstIdx);
        assertEquals(set.getWorth(firstIdx),4);
        assertEquals(set.getIntervalStart(firstIdx),0);
        assertEquals(set.getIntervalEnd(firstIdx),100);
    }
    
    public void testOneValOverlap() {
    	set.add(0,50,1);
    	assertEquals(0,set.getMin());
    	assertEquals(50,set.getMax());
    	set.add(50,110,1);
    	assertEquals(0,set.getMin());
    	assertEquals(110,set.getMax());
    }
    
    public void testValIntervalEndOverlap() {
        set.add(30, 50,4);
        
        assertEquals("min is 30", 30, set.getMin());
        assertEquals("max is 50", 50, set.getMax());
        
        set.add(20,40,5);
        
        // set contains everything between 0 and 100 inclusive
        for (int i=20; i<=50; i++) {
            assertTrue("set contains "+i, set.contains(i));
        }
        
        assertEquals("size is 31", 31, set.size());
        
        int firstIdx = set.getFirstIntervalIndex();
        int lastIdx = set.getLastIntervalIndex();
        int counter=1;
        int curIdx=firstIdx;
        while (curIdx!=lastIdx){
        	curIdx = set.getNextIntervalIndex(curIdx);
        	counter++;
        }
        assertEquals(counter, 3);
        
        assertEquals(set.getWorth(firstIdx),5);
        assertEquals(set.getWorth(firstIdx+1),9);
        assertEquals(set.getWorth(lastIdx),4);
    }
    
//    public void testValIntervalEndOverlapZero() {
//        set.add(30, 50,0);
//        
//        assertEquals("min is 30", 30, set.getMin());
//        assertEquals("max is 50", 50, set.getMax());
//        
//        set.add(20,40,5);
//        
//        // set contains everything between 0 and 100 inclusive
//        for (int i=20; i<=50; i++) {
//            assertTrue("set contains "+i, set.contains(i));
//        }
//        
//        assertEquals("size is 31", 31, set.size());
//        
//        int firstIdx = set.getFirstIntervalIndex();
//        int lastIdx = set.getLastIntervalIndex();
//        int counter=1;
//        int curIdx=firstIdx;
//        while (curIdx!=lastIdx){
//        	curIdx = set.getNextIntervalIndex(curIdx);
//        	counter++;
//        }
//        assertEquals(counter, 2);
//        
//        assertEquals(set.getWorth(firstIdx),5);
//        assertEquals(set.getWorth(lastIdx),0);
//        
//        assertEquals(set.getIntervalStart(firstIdx),20);
//        assertEquals(set.getIntervalEnd(firstIdx),40);
//        
//        assertEquals(set.getIntervalStart(lastIdx),41);
//        assertEquals(set.getIntervalEnd(lastIdx),50);
//    }
    
//    public void testValZeroIntervalEndOverlap() {
//        set.add(30, 50,4);
//        
//        assertEquals("min is 30", 30, set.getMin());
//        assertEquals("max is 50", 50, set.getMax());
//        
//        set.add(20,40,0);
//        
//        // set contains everything between 0 and 100 inclusive
//        for (int i=20; i<=50; i++) {
//            assertTrue("set contains "+i, set.contains(i));
//        }
//        
//        assertEquals("size is 31", 31, set.size());
//        
//        int firstIdx = set.getFirstIntervalIndex();
//        int lastIdx = set.getLastIntervalIndex();
//        int counter=1;
//        int curIdx=firstIdx;
//        while (curIdx!=lastIdx){
//        	curIdx = set.getNextIntervalIndex(curIdx);
//        	counter++;
//        }
//        assertEquals(2,counter);
//        
//        assertEquals(set.getWorth(firstIdx),0);
//        assertEquals(set.getWorth(lastIdx),4);
//        
//        assertEquals(set.getIntervalStart(firstIdx),20);
//        assertEquals(set.getIntervalEnd(firstIdx),29);
//        
//        assertEquals(set.getIntervalStart(lastIdx),30);
//        assertEquals(set.getIntervalEnd(lastIdx),50);
//    }
    
    
    public void testValIntervalOverlapMulti() {
        set.add(10, 20,1);
//        set.add(30, 40,2);
        set.add(50, 60,3);

        assertEquals("min is 10", 10, set.getMin());
        assertEquals("max is 60", 60, set.getMax());
        
        set.add(15,55,5);
        
        // set contains everything between 0 and 100 inclusive
        for (int i=20; i<=50; i++) {
            assertTrue("set contains "+i, set.contains(i));
        }
        
        assertEquals("size is 51", 51, set.size());
        
        int firstIdx = set.getFirstIntervalIndex();
        int lastIdx = set.getLastIntervalIndex();
        int counter=1;
        int curIdx=firstIdx;
        while (curIdx!=lastIdx){
        	curIdx = set.getNextIntervalIndex(curIdx);
        	counter++;
        }
        
        assertEquals(5,counter);
        
        int currentIndex = firstIdx;
        
        assertEquals(set.getWorth(currentIndex),1);
        assertEquals(10,set.getIntervalStart(currentIndex));
        assertEquals(14,set.getIntervalEnd(currentIndex));
        
        currentIndex = set.getNextIntervalIndex(currentIndex);
        
        assertEquals(set.getWorth(currentIndex),6);
        assertEquals(15,set.getIntervalStart(currentIndex));
        assertEquals(20,set.getIntervalEnd(currentIndex));
        
        currentIndex = set.getNextIntervalIndex(currentIndex);
        
        assertEquals(set.getWorth(currentIndex),5);
        assertEquals(21,set.getIntervalStart(currentIndex));
        assertEquals(49,set.getIntervalEnd(currentIndex));
        
        currentIndex = set.getNextIntervalIndex(currentIndex);
        
        assertEquals(set.getWorth(currentIndex),8);
        assertEquals(50,set.getIntervalStart(currentIndex));
        assertEquals(55,set.getIntervalEnd(currentIndex));
        
        currentIndex = set.getNextIntervalIndex(currentIndex);
        
        assertEquals(set.getWorth(currentIndex),3);
        assertEquals(56,set.getIntervalStart(currentIndex));
        assertEquals(60,set.getIntervalEnd(currentIndex));
    }
    
    
    public void testValIntervalOverlapMultig() {
        set.add(10, 20,1);
        set.add(21, 30,3);
        set.add(10,30,2);
        set.add(10,20,2);
        int firstIdx = set.getFirstIntervalIndex();
        int lastIdx = set.getLastIntervalIndex();
        
        assertEquals(firstIdx,lastIdx);
        assertEquals(5,set.getWorth(firstIdx));
        assertEquals(10, set.getIntervalStart(lastIdx));
        assertEquals(30, set.getIntervalEnd(lastIdx));
    }
    
    public void testValIntervalOverlapMultih() {
        set.add(10, 15,3);
        set.add(25, 30,3);
        set.add(10,30,2);
        set.add(16,24,1);
        set.add(16,24,1);
        
        int firstIdx = set.getFirstIntervalIndex();
        int lastIdx = set.getLastIntervalIndex();
        
        assertTrue(firstIdx!=lastIdx);
        set.add(16,24,1);

        firstIdx = set.getFirstIntervalIndex();
        lastIdx = set.getLastIntervalIndex();
        
        assertEquals(firstIdx,lastIdx);
        assertEquals(5,set.getWorth(firstIdx));
        assertEquals(10, set.getIntervalStart(lastIdx));
        assertEquals(30, set.getIntervalEnd(lastIdx));
    }
    
//    public void testFillInTheGaps() {
//        set.add(10, 20,1);
//        set.add(30, 40,2);
//        set.add(50,60,3);
//        set.add(10,60,0);
//        
//        int firstIdx = set.getFirstIntervalIndex();
//        int lastIdx = set.getLastIntervalIndex();
//        int counter=1;
//        int curIdx=firstIdx;
//        while (curIdx!=lastIdx){
//        	curIdx = set.getNextIntervalIndex(curIdx);
//        	counter++;
//        }
//        assertEquals(5,counter);
//        
//        int currentID = firstIdx;
//        
//        assertEquals(1,set.getWorth(currentID));
//        assertEquals(10, set.getIntervalStart(currentID));
//        assertEquals(20, set.getIntervalEnd(currentID));
//        
//        currentID = set.getNextIntervalIndex(currentID);
//        
//        assertEquals(0,set.getWorth(currentID));
//        assertEquals(21, set.getIntervalStart(currentID));
//        assertEquals(29, set.getIntervalEnd(currentID));
//        
//        currentID = set.getNextIntervalIndex(currentID);
//        
//        assertEquals(2,set.getWorth(currentID));
//        assertEquals(30, set.getIntervalStart(currentID));
//        assertEquals(40, set.getIntervalEnd(currentID));
//        
//        currentID = set.getNextIntervalIndex(currentID);
//        
//        assertEquals(0,set.getWorth(currentID));
//        assertEquals(41, set.getIntervalStart(currentID));
//        assertEquals(49, set.getIntervalEnd(currentID));
//        
//        currentID = set.getNextIntervalIndex(currentID);
//        
//        assertEquals(3,set.getWorth(currentID));
//        assertEquals(50, set.getIntervalStart(currentID));
//        assertEquals(60, set.getIntervalEnd(currentID));
//    }
    
    public void testFillInTheGapsMerge() {
        set.add(10, 20,1);
        set.add(30, 40,2);
        set.add(50,60,3);
        set.add(20,75,1);
        
        int firstIdx = set.getFirstIntervalIndex();
        int lastIdx = set.getLastIntervalIndex();
        int counter=1;
        int curIdx=firstIdx;
        while (curIdx!=lastIdx){
        	curIdx = set.getNextIntervalIndex(curIdx);
        	counter++;
        }
        assertEquals(7,counter);
        
        int currentID = firstIdx;
        
        assertEquals(1,set.getWorth(currentID));
        assertEquals(10, set.getIntervalStart(currentID));
        assertEquals(19, set.getIntervalEnd(currentID));
        
        currentID = set.getNextIntervalIndex(currentID);
        
        assertEquals(2,set.getWorth(currentID));
        assertEquals(20, set.getIntervalStart(currentID));
        assertEquals(20, set.getIntervalEnd(currentID));
        
        currentID = set.getNextIntervalIndex(currentID);
        
        assertEquals(1,set.getWorth(currentID));
        assertEquals(21, set.getIntervalStart(currentID));
        assertEquals(29, set.getIntervalEnd(currentID));
        
        currentID = set.getNextIntervalIndex(currentID);
        
        assertEquals(3,set.getWorth(currentID));
        assertEquals(30, set.getIntervalStart(currentID));
        assertEquals(40, set.getIntervalEnd(currentID));
        
        currentID = set.getNextIntervalIndex(currentID);
        
        assertEquals(1,set.getWorth(currentID));
        assertEquals(41, set.getIntervalStart(currentID));
        assertEquals(49, set.getIntervalEnd(currentID));
        
        currentID = set.getNextIntervalIndex(currentID);
        
        assertEquals(4,set.getWorth(currentID));
        assertEquals(50, set.getIntervalStart(currentID));
        assertEquals(60, set.getIntervalEnd(currentID));
        
        currentID = set.getNextIntervalIndex(currentID);
        
        assertEquals(1,set.getWorth(currentID));
        assertEquals(61, set.getIntervalStart(currentID));
        assertEquals(75, set.getIntervalEnd(currentID));
    }
    
    public void testGetMinMaxOverRange() {
        set.add(10, 20,1);
        set.add(30, 40,2);
        set.add(50,60,3);
        set.add(20,75,1);
        assertEquals(4,set.getMaxWorthOverRange(10,65));
        assertEquals(1,set.getMinWorthOverRange(10,65));
        assertEquals(2,set.getMaxWorthOverRange(20,20));
        assertEquals(2,set.getMinWorthOverRange(20,20));
        assertEquals(3,set.getMaxWorthOverRange(10,30));
        assertEquals(1,set.getMinWorthOverRange(10,30));
        assertEquals(3,set.getMaxWorthOverRange(0,30));
        assertEquals(0,set.getMinWorthOverRange(0,30));
    }
    
    public void testGetMergeByRemoval() {
        set.add(10, 20,1);
        set.add(30, 40,2);
        set.add(50,60,3);
        set.add(20,75,3);
                
        int firstIdx = set.getFirstIntervalIndex();
        int lastIdx = set.getLastIntervalIndex();
        int counter=1;
        int curIdx=firstIdx;
        while (curIdx!=lastIdx){
        	curIdx = set.getNextIntervalIndex(curIdx);
        	counter++;
        }
        assertEquals(7,counter);
        
        set.add(27,29,2);
        firstIdx = set.getFirstIntervalIndex();
        lastIdx = set.getLastIntervalIndex();
        counter=1;
        curIdx=firstIdx;
        while (curIdx!=lastIdx){
        	curIdx = set.getNextIntervalIndex(curIdx);
        	counter++;
        }
        assertEquals(7,counter);
        
        set.add(21,26,2);
        firstIdx = set.getFirstIntervalIndex();
        lastIdx = set.getLastIntervalIndex();
        counter=1;
        curIdx=firstIdx;
        while (curIdx!=lastIdx){
        	curIdx = set.getNextIntervalIndex(curIdx);
        	counter++;
        }
        assertEquals(6,counter);
        
        set.remove(21,40,1);
        firstIdx = set.getFirstIntervalIndex();
        lastIdx = set.getLastIntervalIndex();
        counter=1;
        curIdx=firstIdx;
        while (curIdx!=lastIdx){
        	curIdx = set.getNextIntervalIndex(curIdx);
        	counter++;
        }
        assertEquals(5,counter);
        
        set.remove(50,60,3);
        set.add(10,19,3);
        
        set.remove(21,40,1);
        firstIdx = set.getFirstIntervalIndex();
        lastIdx = set.getLastIntervalIndex();
        counter=1;
        curIdx=firstIdx;
        while (curIdx!=lastIdx){
        	curIdx = set.getNextIntervalIndex(curIdx);
        	counter++;
        }
        assertEquals(2,counter);
        
        set.remove(10,20,1);
        firstIdx = set.getFirstIntervalIndex();
        lastIdx = set.getLastIntervalIndex();
        assertEquals(firstIdx,lastIdx);
    }
    
    public void testNormalRemove() {
    	set.add(20,45,3);
    	set.add(60,80,5);
    	int first = set.getFirstIntervalIndex();
    	int last = set.getLastIntervalIndex();
    	set.remove(40,65);
    	assertEquals(80,set.getMax());
    	first = set.getFirstIntervalIndex();
    	last = set.getLastIntervalIndex();
    	assertEquals(3,set.getWorth(first));
    	assertEquals(20,set.getIntervalStart(first));
    	assertEquals(39,set.getIntervalEnd(first));
    	assertEquals(5,set.getWorth(last));
    	assertEquals(66,set.getIntervalStart(last));
    	assertEquals(80,set.getIntervalEnd(last));
    	
    }
    
    public void testSimple() {
    	set.add(10,90,5);
    	set.add(50,75,6);
    	set.add(40,55,7);
        int firstIdx = set.getFirstIntervalIndex();
        int lastIdx = set.getLastIntervalIndex();
        int counter=1;
        int curIdx=firstIdx;
        while (curIdx!=lastIdx){
        	curIdx = set.getNextIntervalIndex(curIdx);
        	counter++;
        }
        assertEquals(5,counter);
    	
    }
    
    public void testEquality() {
    	IntValIntervalSet set1 = new IntValIntervalSet();
    	IntValIntervalSet set2 = new IntValIntervalSet();
    	set1.add(10,90,5);
    	set1.add(50,75,6);
    	set1.add(40,55,7);
    	
    	
    	set2.remove(10,49,6);
    	set2.remove(10,39,7);
    	set2.add(10,90,18);
    	set2.remove(56,90,7);
    	
    	assertFalse(set1.equals(set2));
    	assertFalse(set2.equals(set1));
    	
    	set2.remove(76,90,6);
    	
    	assertTrue(set1.equals(set2));
    	assertTrue(set2.equals(set1));
    	
    	
    }
    
    public void testNoDiffDiff() {
    	IntValIntervalSet set1 = new IntValIntervalSet();
    	IntValIntervalSet set2 = new IntValIntervalSet();
    	set1.add(10,90,5);
    	set1.add(50,75,6);
    	set1.add(40,55,7);
    	
    	
    	set2.remove(10,49,6);
    	set2.remove(10,39,7);
    	set2.add(10,90,18);
    	set2.remove(56,90,7);
    	set2.remove(76,90,6);
    	
    	assertTrue(set1.equals(set2));
    	assertTrue(set2.equals(set1));
    	
    	assertEquals(0,set1.getMinDiff(set2));
    	assertEquals(0,set2.getMinDiff(set1));
    	
    	assertEquals(0,set1.getMaxDiff(set2));
    	assertEquals(0,set2.getMaxDiff(set1));
    }
    
    public void testDiff() {
    	set.add(40,54,7);
    	set.add(23,234,2);
    	
    	IntValIntervalSet set2 = (IntValIntervalSet)set.clone();
    	assertTrue(set.equals(set2));
    	assertTrue(set2.equals(set));
    	
    	assertEquals(0,set.getMinDiff(set2));
    	assertEquals(0,set2.getMinDiff(set));
    	
    	assertEquals(0,set.getMaxDiff(set2));
    	assertEquals(0,set2.getMaxDiff(set));
    	
    	set2.remove(23,200,1);
    	
    	assertEquals(1,set.getMaxDiff(set2));
    	assertEquals(0,set2.getMaxDiff(set));
    	
    	assertEquals(0,set.getMinDiff(set2));
    	assertEquals(-1,set2.getMinDiff(set));
    	
    	set2.add(28,31,13);
    	
    	assertEquals(1,set.getMaxDiff(set2));
    	assertEquals(12,set2.getMaxDiff(set));
    	
    	assertEquals(-12,set.getMinDiff(set2));
    	assertEquals(-1,set2.getMinDiff(set));
    }
    
    public void testDiffRange() {
    	set.add(40,54,7);
    	set.add(23,234,2);
    	
    	IntValIntervalSet set2 = (IntValIntervalSet)set.clone();
    	assertTrue(set.equals(set2));
    	assertTrue(set2.equals(set));
    	
    	assertEquals(0,set.getMinDiff(set2,23,234));
    	assertEquals(0,set2.getMinDiff(set,23,234));
    	
    	assertEquals(0,set.getMaxDiff(set2,23,234));
    	assertEquals(0,set2.getMaxDiff(set,23,234));
    	
    	set2.remove(23,200,1);
    	
    	assertEquals(1,set.getMaxDiff(set2,30,70));
    	assertEquals(0,set.getMaxDiff(set2,210,220));
    	
    	assertEquals(-1,set2.getMaxDiff(set,30,70));
    	assertEquals(0,set2.getMaxDiff(set,210,220));
    	
    	assertEquals(1,set.getMinDiff(set2,30,70));
    	assertEquals(0,set.getMinDiff(set2,210,220));
    	
    	assertEquals(-1,set2.getMinDiff(set,30,70));
    	assertEquals(0,set2.getMinDiff(set,210,220));
    	
    	set2.add(28,31,13);
    	
    	assertEquals(1,set.getMaxDiff(set2));
    	assertEquals(12,set2.getMaxDiff(set));
    	
    	assertEquals(-12,set.getMinDiff(set2));
    	assertEquals(-1,set2.getMinDiff(set));
    	
    	assertEquals(1,set.getMaxDiff(set2,30,70));
    	assertEquals(0,set.getMaxDiff(set2,210,220));
    	
    	assertEquals(12,set2.getMaxDiff(set,30,70));
    	assertEquals(0,set2.getMaxDiff(set,210,220));
    	
    	assertEquals(-12,set.getMinDiff(set2,30,70));
    	assertEquals(0,set.getMinDiff(set2,210,220));
    	
    	assertEquals(-1,set2.getMinDiff(set,30,70));
    	assertEquals(0,set2.getMinDiff(set,210,220));
    	
    }
    
    
    public void testClone() {
    	set.add(40,54,7);
    	set.add(23,234,2);
    	
    	IntValIntervalSet set2 = (IntValIntervalSet)set.clone();
    	assertTrue(set.equals(set2));
    	assertTrue(set2.equals(set));
    }
    
    public void testFindingIntervals() {
    	set.add(30,50,8);
    	set.add(20,35,5);
    	set.add(40,60,4);
    	
    	IntIntervalSet iSet = set.getAllRangesWithMinWorth(11);
    	int firstIndex = iSet.getFirstIntervalIndex();
    	int lastIndex = iSet.getLastIntervalIndex();
    	assertEquals(lastIndex,iSet.getNextIntervalIndex(firstIndex));
    	assertEquals(30,iSet.getIntervalStart(firstIndex));
    	assertEquals(35,iSet.getIntervalEnd(firstIndex));
    	
    	assertEquals(40,iSet.getIntervalStart(lastIndex));
    	assertEquals(50,iSet.getIntervalEnd(lastIndex));
    	
    	
    	iSet = set.getAllRangesWithMaxWorth(7);
    	firstIndex = iSet.getFirstIntervalIndex();
    	lastIndex = iSet.getLastIntervalIndex();
    	assertEquals(lastIndex,iSet.getNextIntervalIndex(firstIndex));
    	assertEquals(20,iSet.getIntervalStart(firstIndex));
    	assertEquals(29,iSet.getIntervalEnd(firstIndex));
    	
    	assertEquals(51,iSet.getIntervalStart(lastIndex));
    	assertEquals(60,iSet.getIntervalEnd(lastIndex));
    }
    
    public void testFindingIntervalsWithRange() {
    	set.add(30,50,8);
    	set.add(20,35,5);
    	set.add(40,60,4);
    	
    	IntIntervalSet iSet = set.getAllRangesWithMinWorth(11,20,38);
    	int firstIndex = iSet.getFirstIntervalIndex();
    	int lastIndex = iSet.getLastIntervalIndex();
    	assertEquals(lastIndex,firstIndex);
    	assertEquals(30,iSet.getIntervalStart(firstIndex));
    	assertEquals(35,iSet.getIntervalEnd(firstIndex));
    	
    	iSet = set.getAllRangesWithMinWorth(11,20,38);
    	firstIndex = iSet.getFirstIntervalIndex();
    	lastIndex = iSet.getLastIntervalIndex();
    	assertEquals(lastIndex,firstIndex);
    	assertEquals(30,iSet.getIntervalStart(firstIndex));
    	assertEquals(35,iSet.getIntervalEnd(firstIndex));
    	
    	iSet = set.getAllRangesWithMaxWorth(7,20,25);
    	firstIndex = iSet.getFirstIntervalIndex();
    	lastIndex = iSet.getLastIntervalIndex();
    	assertEquals(lastIndex,firstIndex);
    	assertEquals(20,iSet.getIntervalStart(firstIndex));
    	assertEquals(25,iSet.getIntervalEnd(firstIndex));
    }
    
    public void testAddingUniqueCondition() {
    	set.add(0,50,1);
    	assertEquals(51,set.size());
    	set.add(50,110,1);
    	assertEquals(111,set.size());
    	set.add(30,70,1);
    	assertEquals(111,set.size());
    	set.remove(101,110,1);
    	assertEquals(101,set.size());
    	
    }
    
    
    
    
    
}